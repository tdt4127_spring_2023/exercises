{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Back to assignment 7](_Oving7.ipynb)\n",
    "# Gaussian Elimination"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "#### Gaussian Elimination with partial pivoting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Gaussian elimination, or row reduction, is an algorithm for solving linear systems presented in augmented matrix form. It works by reducing an augmented matrix to a triangular form, before performing back substitution to obtain solution variables.\n",
    "\n",
    "A linear system, here with three unknowns $x_0,x_1,x_2$ is of the form \n",
    "$$ a_{00}x_0 + a_{01}x_1 + a_{02}x_2 = b_0 $$\n",
    "$$a_{10}x_0 + a_{11}x_1 + a_{12}x_2 = b_1$$\n",
    "$$a_{20}x_0 + a_{21}x_1 + a_{22}x_2 = b_2.$$\n",
    "\n",
    "We can represent it in the more abstract matrix form $Ax=b$, where\n",
    "$$ A = \n",
    "\\begin{bmatrix}\n",
    "a_{00} & a_{01} & a_{02}\\\\\n",
    "a_{10} & a_{11} & a_{12}\\\\\n",
    "a_{20} & a_{21} & a_{22}\n",
    "\\end{bmatrix}, \\quad\n",
    "x = \n",
    "\\begin{bmatrix}\n",
    "x_{0}\\\\\n",
    "x_{1}\\\\\n",
    "x_{2}\n",
    "\\end{bmatrix}, \\quad\n",
    "b = \n",
    "\\begin{bmatrix}\n",
    "b_{0}\\\\\n",
    "b_{1}\\\\\n",
    "b_{2}\n",
    "\\end{bmatrix}.$$\n",
    "\n",
    "\n",
    "To solve a linear system by Gaussian elimination, we first state it in *augmented* form\n",
    "\n",
    "\n",
    "$$\\left[\\begin{array}{ccc|c}\n",
    "a_{00} & a_{01} & a_{02} & b_{0} \\\\\n",
    "a_{10} & a_{11} & a_{12} & b_{1} \\\\\n",
    "a_{20} & a_{21} & a_{22} & b_{2}\n",
    "\\end{array}\\right]$$\n",
    "\n",
    "Then, we reduce it to a triangular form using row operations only, in a systematic fashion. We start with the *pivot column* being column no. 0 and the pivot row being row no. 0. Then we repeat the following pattern:\n",
    "\n",
    "1. Find the maximum entry (in absolute value) in the pivot column from the pivot row to the bottom.\n",
    "    - If this is not possible (i.e. all rows below the pivot row, including the pivot row have 0 in the pivot column), increase the index of the pivot column by 1 and repeat.\n",
    "2. Swap the entries in the pivot row and the row with the maximum value.\n",
    "3. Add multiples of the pivot row to the rows below such that the pivot column has only 0 entries after the pivot row. This means: If the pivot element has value $a_{ij}$ and the element of same column in a later row has value $a_{kj}$, add $\\frac{-a_{kj}}{a_{ij}}$ multiples of the pivot row to that row.\n",
    "4. Increase the numbering of the pivot row and the pivot column by 1.\n",
    "5. If the pivot row is the last row and/or if the pivot column number exceeds the number of columns in the matrix (not counting the extra column with $b_0,b_1,b_2$ ), stop the iterations. Otherwise, repeat from step 1."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "This procedure changes the entries in the matrix, which we in general now call $\\tilde{a}_{ij}$. The matrix should now be in *upper triangular* or *echelon form*:\n",
    "$$\\left[\\begin{array}{ccc|c}\n",
    "\\tilde{a}_{00} & \\tilde{a}_{01} & \\tilde{a}_{02} & \\tilde{b}_{0} \\\\\n",
    "0 & \\tilde{a}_{11} & \\tilde{a}_{12} & \\tilde{b}_{1} \\\\\n",
    "0 & 0 & \\tilde{a}_{22} & \\tilde{b}_{2}\n",
    "\\end{array}\\right].$$\n",
    "\n",
    "This corresponds to the linear system\n",
    "\n",
    "$$\\tilde{a}_{00}x_0 + \\tilde{a}_{01}x_1 + \\tilde{a}_{02}x_2 = \\tilde{b}_0$$\n",
    "$$\\tilde{a}_{11}x_1 + \\tilde{a}_{12}x_2 = \\tilde{b}_1$$\n",
    "$$ \\tilde{a}_{22}x_2 = \\tilde{b}_2.$$\n",
    "\n",
    "\n",
    "We can easily solve this system by backsubstitution, observing that $\\tilde{x}_{2} = \\frac{\\tilde{b}_2}{\\tilde{a}_{22}},$, and that $\\tilde{x}_{1} = \\frac{\\tilde{b}_1 - \\tilde{a}_{12}x_2}{\\tilde{a}_{11}}.$ In general, after reducing the matrix to upper triangular form we can deduce that for a system of $n$ unknowns, $$ x_j = \\frac{\\tilde{b}_j - \\sum_{k = j+1}^{n}\\tilde{a}_{jk}x_k }{\\tilde{a}_{jj}}.$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "**Example** \n",
    "\n",
    "Consider a linear system with the following augmented matrix\n",
    "$$ \\left[\\begin{array}{ccc|c}\n",
    "0 & 1 & 2 & 3 \\\\\n",
    "4 & 5 & 6 & 7 \\\\\n",
    "8 & 9 & 1 & 2\n",
    "\\end{array}\\right].$$\n",
    "From step 2, we swap the first row with the thrid row such that the pivot element is the largest in the pivot column\n",
    "$$\\left[\\begin{array}{ccc|c}\n",
    "8 & 9 & 1 & 2\\\\\n",
    "4 & 5 & 6 & 7 \\\\\n",
    "0 & 1 & 2 & 3 \n",
    "\\end{array}\\right].$$\n",
    "\n",
    "Following step no. 3, we add (-4/8 = -0.5) times the pivot row to row no.1 and (0/4 = 0) times the pivot row to row no. 2 and get\n",
    "$$ \\left[\\begin{array}{ccc|c}\n",
    "8 & 9 & 1 & 2 \\\\\n",
    "0 & 0.5 & 5.5 & 6 \\\\\n",
    "0 & 1 & 2 & 3 \n",
    "\\end{array}\\right].$$\n",
    "\n",
    "Then, from step 4 we increase the numbering of pivot row and column, so our pivot row is 1 and pivot column is also 1. Checking step 5, we do not yet terminate. Returning to step 1, we find the maximal element in column no 1 from row 1 and onward in row 2. Therefore, as in step 2, we swap rows 1 and 2 to get \n",
    "\n",
    "$$ \\left[\\begin{array}{ccc|c}\n",
    "8 & 9 & 1 & 2 \\\\\n",
    "0 & 1 & 2 & 3 \\\\\n",
    "0 & 0.5 & 5.5 & 6 \n",
    "\\end{array}\\right].$$\n",
    "\n",
    "We then follow step 3, adding a multiple of (-0.5/1 = -0.5) of row 1 to row 2, yielding\n",
    "$$\n",
    "\\left[\\begin{array}{ccc|c}\n",
    "8 & 9 & 1 & 2 \\\\\n",
    "0 & 1 & 2 & 3 \\\\\n",
    "0 & 0 & 4.5 & 4.5 \n",
    "\\end{array}\\right].$$\n",
    "\n",
    "After step 4, we have row and column indices of 2, which means we are on the final row and so we stop the iterations after step 5.\n",
    "\n",
    "The above system can be solved by back substitution to find $ x_3 = 1, x_2 = 1, x_1 = -1.$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Assign the variable `matrix` for the augmented matrix of the set of equations\n",
    "\n",
    "$$\n",
    "x - 2y + 1z = 0$$\n",
    "$$\\quad\\quad 2y - 8z = 8$$\n",
    "$$ -4x + 5y + 9z = -9$$\n",
    "\n",
    "***Write code in the block below***"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 1. -2.  1.  0.]\n",
      " [ 0.  2. -8.  8.]\n",
      " [-4.  5.  9. -9.]]\n"
     ]
    }
   ],
   "source": [
    "# SOLUTION\n",
    "\n",
    "import numpy as np\n",
    "\n",
    "mat = np.array([[1.,-2.,1.,0.]\n",
    "                ,[0.,2.,-8.,8.]\n",
    "                ,[-4.,5.,9.,-9.]])\n",
    "\n",
    "print(mat)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## b) \n",
    "Make a function `add` that takes a matrix `A` and adds a multiple of one row to another. The function should accept `A`, two integers `i1` and `i2` as input (the integers specify which rows to add) and a number `num`. The function should take the rows `A[i1,:]` and `A[i2,:]` then add `num*A[i1,:]` to `A[i2,:]`. The function shall not return anything, but only make changes to `A[i2,:]`.\n",
    "\n",
    "**Example run**\n",
    "\n",
    "```python\n",
    "A = np.array([[2.,3.,4.], [5.,-3.,6.]])\n",
    "add(A,0, 1, -5/2) # add (-5/2) times row 0 to row 1\n",
    "print(A)\n",
    "  \n",
    "#Running the code produces the following output:\n",
    "[[  2.    3.    4. ]\n",
    " [  0.  -10.5  -4. ]]\n",
    "```\n",
    "***Write code in the block below***"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[  2.    3.    4. ]\n",
      " [  0.  -10.5  -4. ]]\n"
     ]
    }
   ],
   "source": [
    "# SOLUTION\n",
    "def add(A, i1, i2, num):\n",
    "    \n",
    "    \"\"\"\n",
    "    A = Matrix of interst\n",
    "    i2 = row which we will add to\n",
    "    i1 = Addend (also factor with num)\n",
    "    \n",
    "    \"\"\"\n",
    "    \n",
    "    A[i2,:] = A[i2,:] + A[i1,:]*num \n",
    "    \n",
    "    \n",
    "\n",
    "# Test the code\n",
    "A = np.array([[2.,3.,4.], [5.,-3.,6.]])\n",
    "add(A,0, 1, -5/2) # add (-5/2) times row 0 to row 1\n",
    "\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## c)\n",
    "\n",
    "Make a function `swap` that takes a matrix `A` and two integers `i1` and `i2`, and then swaps the `i1` and `i2` rows of `A`. The function shall not return anything, just swaps the rows in `A`.\n",
    "\n",
    "**Example run**\n",
    "```python\n",
    "A = np.array([[2.,3.,4.], [5.,-3.,6.]])\n",
    "swap(A,0, 1)\n",
    "print(A)\n",
    "  \n",
    "#Running the code produces the following output:\n",
    "[[ 5. -3.  6.]\n",
    " [ 2.  3.  4.]]\n",
    "```\n",
    "***Hint:***\n",
    "you may need to define a temporary variable to save the information in a row before swapping the rows around. E.g.,  `temp = np.array(A[i1,:])`. Also note that you have to have the `np.array` here even tho `A` is already an np.array.\n",
    "\n",
    "***Write code in the block below***\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 5. -3.  6.]\n",
      " [ 2.  3.  4.]]\n"
     ]
    }
   ],
   "source": [
    "# SOLUTION\n",
    "def swap(A,i1,i2):\n",
    "    \"\"\"\n",
    "    Swap row i1 and i2 in matrix A    \n",
    "    \"\"\"\n",
    "    \n",
    "    temp = np.array(A[i1,:])   # Save row i1\n",
    "    A[i1,:] = A[i2,:]          # Replace row i1 with row i2\n",
    "    A[i2,:] = temp             # Replace row i2 with row i1\n",
    "\n",
    "# Test\n",
    "A = np.array([[2.,3.,4.], [5.,-3.,6.]]) \n",
    "swap(A,0, 1)\n",
    "print(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## d)\n",
    "Write a function `getMaxRow` that takes an augmented matrix `A` (with `n` rows and `n+1` columns) and two integers `i` and `j` that specify the row and column of the pivot element. The function should return the index of the row with the largest element below the pivot row. The function should search down through the $j$'th column starting from the $i$'th row and return the *row index* of the element with maximal absolute value. This corresponds to step 1 above.\n",
    "\n",
    "**Example run**\n",
    "```python\n",
    "\n",
    "A = np.array([[0.,1.,2.,3.], [4.,5.,6.,7.], [8.,9.,1.,2.]])\n",
    "print(getMaxRow(A,0,0)) \n",
    "  \n",
    "#In the 0th column, the row with the largest element is in row 2 so the above code should return:\n",
    "#2\n",
    "```\n",
    "\n",
    "***Hint:***\n",
    "\n",
    "If `A` is an n x (n+1) matrix, you can use the built-in function `n = len(A)` to find n. (this returns the row dimension of `A`, regardless of the column dimension)\n",
    "\n",
    "The array `A[i:n,j]` returns the elements in the column below and including the pivot element.\n",
    "\n",
    "***write code in block below***\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# SOLUTION\n",
    "def getMaxRow(A,i,j):    \n",
    "    \n",
    "    \"\"\"\n",
    "    A = matrix of interst\n",
    "    i = row index\n",
    "    j = col index\n",
    "    \n",
    "    \"\"\"\n",
    "    n_rows = len(A)                     # Get number of rows in A \n",
    "    pivCol = np.array(A[i:n_rows,j])    # Get all the elements in the pivot column below the pivot row \n",
    "    maxNum = abs(A[i,j])                # The abs value of the largest element (we start with the pivot element then will update this accordingly)\n",
    "    maxPos = i                          # The row index of the max element (which we will update)\n",
    "\n",
    "\n",
    "    for k in range(i+1,n_rows):         # Search A[i:n,j] for elements larger than the pivot  \n",
    "        \n",
    "        if abs(A[k,j]) > maxNum:        # If A[k,j] is larger than the current maxNum then update maxNum and maxPos\n",
    "            maxNum = A[k,j]\n",
    "            maxPos = k\n",
    "            \n",
    "    return maxPos                       # We have searched all of A[i:n,j], return the maxPos\n",
    "\n",
    "\n",
    "#* Test code:\n",
    "\n",
    "A = np.array([[1.,1.,2.,3.], [4.,5.,6.,7.], [0.,9.,1.,2.]])\n",
    "\n",
    "getMaxRow(A,0,1)\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## e)\n",
    "\n",
    "Write a function `rowOps` that takes as arguments a matrix `A` and two index numbers `i` and `j` that specify the pivot element. The function should first check whether `A[i,j]==0`, and return without doing anything if true. Otherwise, the function should add appropriate multiples of `A[i,:]` to each row with index larger than `i` in `A`, such that its entry with index `j` (i.e., in the pivot row) is set to zero.  This corresponds to step 3 above.\n",
    "\n",
    "**example run**\n",
    "```python\n",
    "#Example 1:\n",
    "A = np.array([[0.,1.,1.,3.], [1.,2,3,0], [1.,3.,4.,-2.]])\n",
    "rowOps(A,0,0)\n",
    "print(A)\n",
    "  \n",
    "#Running the code produces the following output:\n",
    "[[ 0.  1.  1.  3.]\n",
    " [ 1.  2.  3.  0.]\n",
    " [ 1.  3.  4. -2.]]\n",
    "#Nothing is changed since A[0,0]=0.\n",
    "  \n",
    "  \n",
    "#Example 2\n",
    "A = np.array([[8.,1.,1.,3.], [2.,6.,3.,0.], [4.,3.,4.,-2.]])\n",
    "rowOps(A,0,0)\n",
    "print(A)\n",
    "  \n",
    "#Running the code in example 2 produces the following output:\n",
    "[[ 8.    1.    1.    3.  ]\n",
    " [ 0.    5.75  2.75 -0.75]\n",
    " [ 0.    2.5   3.5  -3.5 ]]\n",
    "```\n",
    "***Hint:*** \n",
    "\n",
    "Remember to use floats in your matrix `A` and not integers otherwise you might get the wrong result as arithmatic with integers are rounded, which is not what we want. This is done by putting a decimal point after each number (that is, we write `2.` instead of `2`).\n",
    "\n",
    "***Write code in block below*** "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[  0.   1.   1.   3.]\n",
      " [  1.   0.   1.  -6.]\n",
      " [  1.   0.   1. -11.]]\n",
      "[[  8.   1.   1.   3.]\n",
      " [-46.   0.  -3. -18.]\n",
      " [-20.   0.   1. -11.]]\n"
     ]
    }
   ],
   "source": [
    "# SOLUTION\n",
    "def rowOps(A,i,j):\n",
    "    \"\"\"\n",
    "    Add multiples of the pivot row to the rows below such that the pivot column has only 0 entries after the pivot row.\n",
    "    \"\"\"\n",
    "    n = len(A[:,0]) # Number rows\n",
    "    \n",
    "    if A[i,j] == 0: # Evaluate the pivot element. Do nothing if = 0 \n",
    "        return\n",
    "    else:\n",
    "        for k in range(i+1,n): # Pluss one to start adding at the next row\n",
    "            add(A,i,k,-A[k,j]/A[i,j]) # Using previos function to add row multiples\n",
    "\n",
    "# Test:     \n",
    "A = np.array([[0.,1.,1.,3.], [1.,2,3,0], [1.,3.,4.,-2.]])\n",
    "rowOps(A,0,1)\n",
    "print(A)\n",
    "  \n",
    "A = np.array([[8.,1.,1.,3.], [2.,6.,3.,0.], [4.,3.,4.,-2.]])\n",
    "rowOps(A,0,1)\n",
    "print(A)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "#### Hint"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Use the add function implemented in b) to add rows\n",
    "\n",
    "Use a for loop that takes you over all the rows below the pivot row\n",
    "\n",
    "If the pivot column has index `j` and the pivot row has index `i`, you should add (`-A[k,j]/A[i,j]`) times the pivot row to zero out row number `k`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## f)\n",
    "Now make a function `Gauss` that takes a matrix `A` as and uses `getMaxRow`, `rowOps` and `swap` functions to perform a Gaussian elimination with partial pivoting on `A`.\n",
    "\n",
    "**Example run:**\n",
    "```python\n",
    "A = np.array([[0.,1.,2.,3.], [4.,5.,6.,7.], [8.,9.,1.,2.]])\n",
    " \n",
    "Gauss(A)\n",
    "print(A)\n",
    "  \n",
    "#Running the code produces the following output\n",
    "[[8.  9.  1.  2. ]\n",
    " [0.  1.  2.  3. ]\n",
    " [0.  0.  4.5 4.5]]\n",
    "```\n",
    "\n",
    "***Hint***\n",
    "\n",
    "There are many ways to approach this problem. One possible way is with a for loop: `for i in range(0,n):` where `n` is the row dimension of `A`\n",
    "\n",
    "You should use your functions `getMaxRows`, `swap` and `rowOps` within the loop. \n",
    "\n",
    "Make use of the pseudocode (steps 1-5) above.\n",
    "\n",
    "Every time you use row_ops, increase both the row and column indices of your pivot element.\n",
    "\n",
    "***write code in block below***"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[8.  9.  1.  2. ]\n",
      " [0.  1.  2.  3. ]\n",
      " [0.  0.  4.5 4.5]]\n"
     ]
    }
   ],
   "source": [
    "# SOLUTION\n",
    "def Gauss(A):\n",
    "    \"\"\"\n",
    "    Complete code for conducting Gaussian elimination on matrix A.\n",
    "    \"\"\"\n",
    "    n_rows = len(A)             # Variable to store number of rows\n",
    "    for i in range(0,n_rows):\n",
    "        imax = getMaxRow(A,i,i) # Identify where the largest element in the pivot column is\n",
    "        swap(A,i,imax)          # Swap the initial row with the row with the largest element.\n",
    "        rowOps(A,i,i)           # Start to add multiples to make the rest of the pivot elements zero. \n",
    "\n",
    "# Test:        \n",
    "A = np.array([[0.,1.,2.,3.], [4.,5.,6.,7.], [8.,9.,1.,2.]])\n",
    "Gauss(A)\n",
    "print(A)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Bonus question! (optional) \n",
    "\n",
    "Make a function  `backSubs` that takes as input a matrix `A` in *echelon form*, and performs *back substitution*, returning a list containing the solution to the system.\n",
    "\n",
    "Call the functions `Gauss` and `backSubs` on the matrix from a), and print the results.\n",
    "\n",
    "**example run**\n",
    "```python\n",
    "#Example 1\n",
    "A = np.array([[1.,1.,1.,3.], [1.,2.,3.,0.], [1.,3.,4.,-2.]])\n",
    "Gauss(A)\n",
    "x = backSubs(A)\n",
    "print(x)\n",
    "  \n",
    "#Running this code produces this output:\n",
    "[ 5. -1. -1.]\n",
    "```\n",
    "**Hint:**\n",
    "Remember a sum should be implemented using a `for` loop. For example $$c = \\sum_{k=0}^{10} 2^{-n}$$ is implemented in Python via \n",
    "```python\n",
    "c = 0\n",
    "for k in range(0,10):\n",
    "    c = c + 2**(-k)\n",
    "    \n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ 5. -1. -1.]\n"
     ]
    }
   ],
   "source": [
    "# SOLUTION\n",
    "def backSubs(A):\n",
    "    \n",
    "    n_rows = len(A)                     # Number of rows\n",
    "    x = np.zeros(n_rows)                # Matrix to store our solutions\n",
    "    for j in reversed(range(0,n_rows)): # Reverse for loop to start at the last row hence backsubstitution. \n",
    "        sum = 0\n",
    "        \n",
    "        for k in range(j+1,n_rows):\n",
    "            sum = sum + A[j,k]*x[k]\n",
    "        x[j] = (A[j,-1]-sum)/A[j,j]\n",
    "    return x\n",
    "\n",
    "A = np.array([[1.,1.,1.,3.], [1.,2.,3.,0.], [1.,3.,4.,-2.]])\n",
    "Gauss(A)\n",
    "x = backSubs(A)\n",
    "print(x)\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
